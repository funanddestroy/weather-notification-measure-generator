package com.app.sensors;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Pong;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Random;

@Component
public class MeasureGenerator {

    private @Value("${database.url}") String url;
    private @Value("${database.username}") String username;
    private @Value("${database.password}") String password;
    private @Value("${database.database}") String database;

    private InfluxDB influxDB;
    private Random random;

    @PostConstruct
    private void init() {
        influxDB = InfluxDBFactory.connect(url, username, password);
        Pong response = influxDB.ping();
        if (response.getVersion().equalsIgnoreCase("unknown")) {
            System.out.println("Error pinging server.");
            return;
        }

        random = new Random();
    }

    @Scheduled(cron = "0 0/5 * * * ?")
    private void generate() {

        BatchPoints batchPoints = BatchPoints
                .database(database)
                .build();

        Point point = Point.measurement("measure")
                .addField("temperature", random.nextInt(80) - 40)
                .addField("humidity", random.nextInt(100))
                .addField("pressure", random.nextInt(40) + 740)
                .addField("wind_speed", random.nextInt(80))
                .addField("latitude", 51.539137)
                .addField("longitude", 46.010916)
                .build();

        batchPoints.point(point);
        influxDB.write(batchPoints);
        try {
            Thread.sleep(9000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
